//
//  JLTestAPIManager.swift
//  JLTestTests
//
//  Created by mannyjammydev on 18/06/2018.
//  Copyright © 2018 mannyjammydev. All rights reserved.
//

import XCTest
@testable import JLTest

class JLTestJLRequest: XCTestCase {
    
    var systemUnderTest: JLRequest!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        systemUnderTest = JLRequest.search(forItems: "dishwasher")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        systemUnderTest = nil
        super.tearDown()
    }
    
    func testValidURL() {
        XCTAssertNotNil(systemUnderTest.url, "Unable to create valid URL")
    }
    
    func testsValidHost() {
        
        guard let url = systemUnderTest.url, let comps = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            XCTFail("Invalid url \(String(describing: systemUnderTest.url))")
            return
        }
        
        let result = comps.host?.caseInsensitiveCompare("api.johnlewis.com")
        
        XCTAssertEqual(result, ComparisonResult.orderedSame, "Invalid API Host")
    }
    
    func testsValidProtocol() {
        
        guard let url = systemUnderTest.url, let comps = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            XCTFail("Invalid url \(String(describing: systemUnderTest.url))")
            return
        }
        
        let result = comps.scheme?.caseInsensitiveCompare("https")
        
        XCTAssertEqual(result, ComparisonResult.orderedSame, "Invalid API Protocol")
    }
    
    func testsValidAPIKey() {
        
        guard let url = systemUnderTest.url, let comps = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            XCTFail("Invalid url \(String(describing: systemUnderTest.url))")
            return
        }

        guard let params = comps.queryItems else {
            XCTFail("Invalid query params \(String(describing: comps.queryItems))")
            return
        }
        
        let values = params.filter { $0.name.caseInsensitiveCompare("key") == .orderedSame }
        
        guard let item = values.first, let keyValue = item.value else {
            XCTFail("API Key not found")
            return
        }
        
        let result = keyValue.compare("Wu1Xqn3vNrd1p7hqkvB6hEu0G9OrsYGb")
        
        XCTAssertEqual(result, ComparisonResult.orderedSame, "Invalid API Key")
    }
    
}
