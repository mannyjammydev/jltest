//
//  JLTestDataParser.swift
//  JLTestTests
//
//  Created by mannyjammydev on 20/06/2018.
//  Copyright © 2018 mannyjammydev. All rights reserved.
//

import XCTest

@testable import JLTest

class JLTestDataParser: XCTestCase {
    
    var systemUnderTest: Data!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let sampleDataPath = Bundle(for: type(of: self)).path(forResource: "search_dishwasher_response", ofType: "json")!
        
        systemUnderTest = try! Data(contentsOf: URL(fileURLWithPath: sampleDataPath))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        systemUnderTest = nil
    }
    
    func testParseEmptyDataThrowsException() {
    
        //Given
        let emptyData = " ".data(using: .utf8)!
        
        // When
        XCTAssertThrowsError(try DataParser.parse(emptyData))
    
    }
    
    func testParseSampleDataNoThrow() {
        
        XCTAssertNoThrow(try DataParser.parse(systemUnderTest))
    }
    
    func testParseSampleDataContainsResults() {

        let result = try! DataParser.parse(systemUnderTest)

        XCTAssertGreaterThan(result.results, 0)
    }
    
    func testParseSampleDataContainsProducts() {
        
        let result = try! DataParser.parse(systemUnderTest)
        
        XCTAssertGreaterThan(result.products.count, 0)
    }
    
    func testParseSampleDataContainsProductWithPrice() {
        
        let items = try! DataParser.parse(systemUnderTest)
        
        let result = items.products.first!
        
        XCTAssertGreaterThan(result.price.now.count, 0)
    }
}
