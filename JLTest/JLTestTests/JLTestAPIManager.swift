//
//  JLTestTests.swift
//  JLTestTests
//
//  Created by mannyjammydev on 18/06/2018.
//  Copyright © 2018 mannyjammydev. All rights reserved.
//

import XCTest

@testable import JLTest

class JLTestAPIManager: XCTestCase {
    
    var sutRequest: JLRequest!
    var sutAPIManager: APIManager!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        sutRequest = JLRequest.search(forItems: "dishwasher")
        sutAPIManager = APIManager.shared    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sutAPIManager = nil
        sutRequest = nil
        
        super.tearDown()
    }
    
    func testValidConnection() {
        
        // Create an expectation
        let expectation = self.expectation(description: "Connection")
        
        sutAPIManager.fetch(using: sutRequest) { result in
            
            expectation.fulfill()
            
        }
        
        waitForExpectations(timeout: 30, handler: nil)
        
    }
    
    func testInvalidProductImageFetch() {
        
        let invalidProduct = Product(productId: "", title: "", image: "", price: Price(now: "", currency: ""))
        
        var result: Result<UIImage>?
        let expectation = self.expectation(description: "Image Failure")
        
        sutAPIManager.fetchImage(for: invalidProduct) { (imageResult) in
            result = imageResult
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
        
        guard let _result = result else {
            XCTFail()
            return
        }
        
        switch _result {
        case .success(_):
            XCTFail("A valid image should not exist")
        default: break
        }
        
    }
    
    func testValidProductImageFetch() {
        
        let product = Product(productId: "12345", title: "Sample", image: "//johnlewis.scene7.com/is/image/JohnLewis/236888507", price: Price(now: "1234", currency: "USD"))
        
        var result: Result<UIImage>?
        let expectation = self.expectation(description: "Image Success")
        
        sutAPIManager.fetchImage(for: product) { (imageResult) in
            result = imageResult
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
        
        guard let _result = result else {
            XCTFail()
            return
        }
        
        switch _result {
        case .error(_):
            XCTFail("A valid image should have been created")
        default: break
        }
        
    }
    
    func DISABLED_testValidData() {
        
        // Create an expectation
//        let expectation = self.expectation(description: "Connection")
//
//        var someData: [String: AnyObject]?
//
//        sutAPIManager.fetch(using: sutRequest) { result in
//
//            switch result {
//            case .success(let data):
//                someData = data
//                expectation.fulfill()
//                break;
//            case .error(let message):
//                XCTFail(message)
//                break;
//            }
//
//        }
//
//        waitForExpectations(timeout: 30, handler: nil)
//
//        XCTAssertNotNil(someData)
        
    }
    
    func DISABLED_testValodDataCount() {
        
//        // Create an expectation
//        let expectation = self.expectation(description: "Connection")
//
//        var someData: [String: AnyObject]!
//
//        sutAPIManager.fetch(using: sutRequest) { result in
//
//            switch result {
//            case .success(let data):
//                someData = data
//                expectation.fulfill()
//                break;
//            case .error(let message):
//                XCTFail(message)
//                break;
//            }
//
//        }
//
//        waitForExpectations(timeout: 30, handler: nil)
//
//        XCTAssertGreaterThanOrEqual(someData.count, 1, "No Items Found")
    }
    
    func DISABLED_testValidDataItemsCount() {
        
//        // Create an expectation
//        let expectation = self.expectation(description: "Connection")
//        
//        var someData: [String: AnyObject]!
//        
//        sutAPIManager.fetch(using: sutRequest) { result in
//            
//            switch result {
//            case .success(let data):
//                someData = data
//                expectation.fulfill()
//                break;
//            case .error(let message):
//                XCTFail(message)
//                break;
//            }
//            
//        }
//        
//        waitForExpectations(timeout: 30, handler: nil)
//        
//        guard let products =  someData["products"] else {
//            XCTFail("No products found")
//            return
//        }
//        
//        XCTAssertGreaterThanOrEqual(products.count, 20, "No Incorrect number of items found")
    }
    
}

