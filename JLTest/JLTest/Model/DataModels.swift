//
//  DataModels.swift
//  JLTest
//
//  Created by mannyjammydev on 20/06/2018.
//  Copyright © 2018 mannyjammydev. All rights reserved.
//

import Foundation

struct Price {
    
    var now: String
    var currency: String
    
}

extension Price: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case now, currency
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        now = try values.decode(String.self, forKey: .now)
        currency = try values.decode(String.self, forKey: .currency)
    }
    
}

struct Product {
    
    var productId: String
    var title: String
    var image: String
    var price: Price
}

extension Product: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case productId, title, image, price
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        productId = try values.decode(String.self, forKey: .productId)
        title = try values.decode(String.self, forKey: .title)
        image = try values.decode(String.self, forKey: .image)
        price = try values.decode(Price.self, forKey: .price)
    }
}

struct ProductSearch {
    
    var products: [Product]
    var results: Int
    
}

extension ProductSearch: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case products, results
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        products = try values.decode([Product].self, forKey: .products)
        results = try values.decode(Int.self, forKey: .results)
    }
}
