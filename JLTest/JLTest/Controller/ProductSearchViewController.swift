//
//  ViewController.swift
//  JLTest
//
//  Created by mannyjammydev on 18/06/2018.
//  Copyright © 2018 mannyjammydev. All rights reserved.
//

import UIKit

class ProductSearchViewController: UIViewController {

    @IBOutlet var productsCollectionView: UICollectionView!
    
    private var products: [Product]? {
        didSet {
            DispatchQueue.main.async {
                self.productsCollectionView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        productsCollectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ProductCollectionViewCell.resueID)
        
        APIManager.shared.imageCache = NSCache<NSString, UIImage>()
        
        fetchData()
    }
    
    func fetchData() {
        
        let fetchDishwashers = JLRequest.search(forItems: "dishwasher")
        
        APIManager.shared.fetch(using: fetchDishwashers) { result in
            
            switch result {
            case .error(let errortext):
                print(errortext)
            case .success(let data):
                do {
                    let results = try DataParser.parse(data)
                    self.products = results.products
                    DispatchQueue.main.async {
                        self.navigationItem.title = "Products \(results.products.count)"
                    }
                } catch let parseError {
                    print(parseError.localizedDescription)
                }
            }
            
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        productsCollectionView.collectionViewLayout.invalidateLayout()
    }
    
}

extension ProductSearchViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.resueID, for: indexPath) as! ProductCollectionViewCell
        
        guard let product = products?[indexPath.row] else { return cell }
        cell.configure(using: product)
        
        return cell
    }
}

extension ProductSearchViewController: UICollectionViewDelegate {
    // For completeness
}

extension ProductSearchViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemsPerRow = UIInterfaceOrientationIsPortrait(UIApplication.shared.statusBarOrientation) ? 3 : 4
        
        let cellWidth = (collectionView.frame.size.width - 10) / CGFloat(itemsPerRow)
        
        return CGSize(width: cellWidth, height: cellWidth + 140)
    }
    
}
