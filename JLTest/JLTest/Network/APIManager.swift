//
//  APIManager.swift
//  JLTest
//
//  Created by mannyjammydev on 18/06/2018.
//  Copyright © 2018 mannyjammydev. All rights reserved.
//

import Foundation
import UIKit.UIImage

enum APIError {
    static let invalidURL = "Invalid URL"
    static let unableToCreateURL = "Unable to create a valid URL"
    static let unknown = "Undefined expection"
}

enum Result<T> {
    case success(T)
    case error(String)
}

class JLRequest {
    
    fileprivate init(path: String? = nil, params: [URLQueryItem]? = nil) {
        self.extraPath = path
        self.extraQueryItems = params
    }
    
    private let extraPath: String?
    
    private let extraQueryItems: [URLQueryItem]?
    
    private lazy var urlComponents: URLComponents = {
        
        var components = URLComponents()
        components.host = "api.johnlewis.com"
        components.scheme = "https"
        components.path = "/v1/products/"
        let apiKey = URLQueryItem(name: "key", value: "Wu1Xqn3vNrd1p7hqkvB6hEu0G9OrsYGb")
        components.queryItems = [apiKey]
        return components
    } ()
    
    var url: URL? {
        
        var urlcomps = urlComponents
        
        if let ePath = extraPath {
            urlcomps.path += ePath
        }
        
        if let qParams = extraQueryItems {
            urlcomps.queryItems = (urlcomps.queryItems ?? []) + qParams
        }
        
        return urlcomps.url
    }
    
    static func search(forItems named: String) -> JLRequest {
        
        let searchParams = URLQueryItem(name: "q", value: named)
        
        let itemCount = URLQueryItem(name: "pageSize", value: "20")
        
        return JLRequest(path: "search", params: [searchParams, itemCount])
    }
    
}

class APIManager {
    
    static let shared = APIManager()
    
    private init () {}
    
    var imageCache: NSCache<NSString,UIImage>?
    
    func fetch(using request: JLRequest, completion: @escaping (Result<Data>) -> Swift.Void) {

        guard let url = request.url else { return completion(.error(APIError.invalidURL)) }
        
        let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let error = error {
                completion(.error(error.localizedDescription))
            } else if let data = data {
                completion(.success(data))
            } else {
                completion(.error(APIError.unknown))
            }
            
        }
        
        dataTask.resume()
    }
    
    func fetchImage(for product: Product, completion: @escaping (Result<UIImage>) -> Swift.Void) {
        
        if let cachedImage = imageCache?.object(forKey: product.image as NSString) {
            return completion(.success(cachedImage))
        }
        
        guard var urlComps = URLComponents(string: product.image) else {
            return completion(.error(APIError.invalidURL))
        }
        
        urlComps.scheme = "https"
        
        guard let url = urlComps.url else {
            return completion(.error(APIError.unableToCreateURL))
        }
        
        let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let error = error {
                completion(.error(error.localizedDescription))
            } else if let data = data, let image = UIImage(data: data) {
                self.imageCache?.setObject(image, forKey: product.image as NSString)
                completion(.success(image))
            } else {
                completion(.error(APIError.unknown))
            }
        }
        
        dataTask.resume()
    }
    
}
