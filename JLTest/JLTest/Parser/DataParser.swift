//
//  DataParser.swift
//  JLTest
//
//  Created by mannyjammydev on 20/06/2018.
//  Copyright © 2018 mannyjammydev. All rights reserved.
//

import Foundation

struct DataParser {
    
    static func parse(_ data: Data) throws -> ProductSearch {
        return try JSONDecoder().decode(ProductSearch.self, from: data)
    }
    
}
