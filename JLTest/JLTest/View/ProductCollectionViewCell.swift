//
//  ProductCollectionViewCell.swift
//  JLTest
//
//  Created by mannyjammydev on 20/06/2018.
//  Copyright © 2018 mannyjammydev. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.image = #imageLiteral(resourceName: "product_image_placeholder")
    }
    
    static var resueID: String = {
        return String(describing: type(of: self))
    } ()
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var informationLabel: UILabel!
    
    func configure(using product: Product) {
        
        let currencySymbol = symbolForCurrencyCode(product.price.currency) ?? ""
        let priceNow = "\(currencySymbol)\(product.price.now)"
        let details = "\(product.title)\n\(priceNow)"
        
        let attString = NSMutableAttributedString(string: details)
        attString.addAttribute(NSAttributedStringKey.font, value: UIFont.boldSystemFont(ofSize: 18),range:(details  as NSString).range(of: priceNow))
        informationLabel.attributedText = attString
        
        APIManager.shared.fetchImage(for: product) { (result) in
            
            switch result {
            case .error(let text): print(text)
            case .success(let image):
                DispatchQueue.main.async {
                    self.imageView.image = image
                }
            }
            
        }
        
    }
    
    private func symbolForCurrencyCode(_ code: String) -> String?
    {
        let locale = NSLocale(localeIdentifier: code)
        return locale.displayName(forKey: NSLocale.Key.currencySymbol, value: code)
    }

}
